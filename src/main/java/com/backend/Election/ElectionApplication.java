package com.backend.Election;

import com.backend.Election.Models.ListesElectoral.ListeCoalation;
import com.backend.Election.Models.ListesElectoral.ListePartisienne;
import com.backend.Election.Repos.IRepoListeCoalation;
import com.backend.Election.Repos.IRepoListeElectoral;
import com.backend.Election.Repos.IRepoListePartisienne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })

public class ElectionApplication  {
	public static void main(String[] args) {
		SpringApplication.run(ElectionApplication.class, args);
	}

}
