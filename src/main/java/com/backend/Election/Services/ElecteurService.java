package com.backend.Election.Services;
import com.backend.Election.Models.Users.Electeur;
import com.backend.Election.Repos.IRepoElecteur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ElecteurService {
    @Autowired
    private IRepoElecteur _irepo;

    public ElecteurService(IRepoElecteur _irepo){this._irepo=_irepo;}

    public ResponseEntity<?>getAllElecteur()
    {
        List<Electeur> list=_irepo.findAll();
        if (list.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(list,HttpStatus.OK);
    }

    public ResponseEntity<?> getElecteurById(String id)
    {
        Optional<Electeur> electeur = _irepo.findById(id);
        if (electeur.isEmpty())
            return  new ResponseEntity(HttpStatus.NOT_FOUND);
        return new ResponseEntity(electeur, HttpStatus.OK);
    }

    public ResponseEntity<Electeur> deleteElecteur(String id)
    {
        Optional<Electeur> electeur= _irepo.findById(id);
        if (electeur.isEmpty())
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        _irepo.deleteById(id);
        return new
                ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<Electeur> addElecteur(Electeur e1)
    {
        List<Electeur> ElecteurMac= _irepo.findByMac(e1.getAdresse_Mac());



        if(!ElecteurMac.isEmpty())
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        _irepo.save(e1);
        return new ResponseEntity(e1,HttpStatus.OK);
    }

    public ResponseEntity<Electeur> updateElecteur(String id,Electeur newElecteur)
    {
        Optional<Electeur> Electeur =_irepo.findById(id);


        if(Electeur.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        List<Electeur> listeVerifMac=_irepo.findByMac(newElecteur.getAdresse_Mac());
        Electeur electeur1= Electeur.get();

        if(!listeVerifMac.isEmpty()&&!(listeVerifMac.get(0).get_id().equals(id)))
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        electeur1.setNom(newElecteur.getNom());
        electeur1.setPrenom(newElecteur.getPrenom());
        electeur1.setAdresse_Mac(newElecteur.getAdresse_Mac());


        electeur1.setDate_naissance(newElecteur.getDate_naissance());






        Electeur e1=_irepo.save(electeur1);
        return new ResponseEntity(e1,HttpStatus.OK);

    }


}
