package com.backend.Election.Services;



import com.backend.Election.Models.Users.Admin;
import com.backend.Election.Repos.IRepoAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class AdminService {
    @Autowired
    private IRepoAdmin _irepo;

    public AdminService(IRepoAdmin _irepo) {
        this._irepo = _irepo;
    }
    public ResponseEntity<Admin> getAll(){
        Collection<Admin> listAdmins= _irepo.findAll();
        if(listAdmins.isEmpty())
            return  new ResponseEntity(HttpStatus.NOT_FOUND);
        return new ResponseEntity(listAdmins,HttpStatus.OK);
    }
    public ResponseEntity<?> getById(String id){
        Optional<Admin> admin=_irepo.findById(id);
        if (admin.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(admin,HttpStatus.OK);
    }
    public ResponseEntity<Admin> deleteAdmin(String id){
        Optional<Admin> admin= _irepo.findById(id);
        if (admin.isEmpty())
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        _irepo.deleteById(id);
        return new
                ResponseEntity<>(HttpStatus.OK);
    }
    public ResponseEntity<Admin> addAdmin(Admin a1){
        List<Admin> listAdmins= _irepo.findByUsername(a1.getUsername());

        if(!listAdmins.isEmpty())
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        _irepo.save(a1);
        return new ResponseEntity(a1,HttpStatus.OK);
    }
    public ResponseEntity<Admin> updateAdmin(String id , Admin newAdmin){
        Optional<Admin> admin=_irepo.findById(id);
        if(admin.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        List<Admin> adminUsernameVerif=_irepo.findByUsername(newAdmin.getUsername());
        if(!adminUsernameVerif.isEmpty()&&!(adminUsernameVerif.get(0).get_id().equals(id)))
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        Admin admin1= admin.get();
        admin1.setUsername(newAdmin.getUsername());
        admin1.setPassword(newAdmin.getPassword());
        admin1.setRole(newAdmin.getRole());

        Admin a1=_irepo.save(admin1);
        return new ResponseEntity(a1,HttpStatus.OK);





    }



}
