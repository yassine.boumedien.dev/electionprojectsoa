package com.backend.Election.Services;

import com.backend.Election.Models.ListesElectoral.ListeElectoral;
import com.backend.Election.Repos.IRepoListeElectoral;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ListeElectoralService {

    @Autowired
    private IRepoListeElectoral _repo;

    public ListeElectoralService(IRepoListeElectoral repo){
        _repo=repo;
    }
    public ResponseEntity<?> GetAll(){
        List<ListeElectoral> liste=_repo.findAll();

        if (liste.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(liste,HttpStatus.OK);
    }
    public ResponseEntity<?> GetById(String id){
        Optional<ListeElectoral> liste=_repo.findById(id);
        if (liste.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(liste,HttpStatus.OK);
    }

    public ResponseEntity<?> Delete(String id){

        Optional<ListeElectoral> liste=_repo.findById(id);
        if (liste.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        _repo.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }





}
