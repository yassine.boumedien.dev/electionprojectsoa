package com.backend.Election.Services;

import com.backend.Election.Models.Utils.Reclamation;
import com.backend.Election.Repos.IRepoReclamation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReclamationService {
    @Autowired
    private IRepoReclamation _irepo;
    public ReclamationService(IRepoReclamation _irepo){this._irepo=_irepo;}

    public ResponseEntity<?>getAllReclamation()
    {
        List<Reclamation> rec=_irepo.findAll();
        if (rec.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(rec,HttpStatus.OK);
    }

    public ResponseEntity<?> getReclamationById(String id)
    {
        Optional<Reclamation> rec = _irepo.findById(id);
        if (rec.isEmpty())
            return  new ResponseEntity(HttpStatus.NOT_FOUND);
        return new ResponseEntity(rec, HttpStatus.OK);
    }

    public ResponseEntity<Reclamation> deleteReclamation(String id)
    {
        Optional<Reclamation> rec= _irepo.findById(id);
        if (rec.isEmpty())
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        _irepo.deleteById(id);
        return new
                ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<Reclamation> addReclamation(Reclamation r1)
    {
        _irepo.save(r1);
        return new ResponseEntity(r1,HttpStatus.OK);
    }

    public ResponseEntity<Reclamation> updateReclamation(String id,Reclamation newReclamation)
    {
        Optional<Reclamation> rec =_irepo.findById(id);


        if(rec.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        Reclamation reclamation1= rec.get();
        reclamation1.setTitre(newReclamation.getTitre());
        reclamation1.setDescription(newReclamation.getDescription());
        reclamation1.setStatus(newReclamation.getStatus());


        reclamation1.getSupports().clear();
        for(var x:newReclamation.getSupports())
        {
            reclamation1.getSupports().add(x);
        }

        Reclamation r1=_irepo.save(reclamation1);
        return new ResponseEntity(r1,HttpStatus.OK);

    }


}
