package com.backend.Election.Services;

import com.backend.Election.Models.Candidat;
import com.backend.Election.Models.ListesElectoral.ListeElectoral;
import com.backend.Election.Models.Users.Electeur;
import com.backend.Election.Models.Utils.Reclamation;
import com.backend.Election.Repos.IRepoCandidat;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CandidatService {
    @Autowired
    private IRepoCandidat _irepo;

    public CandidatService(IRepoCandidat irepo){
        _irepo=irepo;
    }

    public ResponseEntity<?> getAllCandidat(){
        List<Candidat> list=_irepo.findAll();
        if (list.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(list,HttpStatus.OK);
    }

    public ResponseEntity<?> getCandidatById(String id){
        Optional<Candidat> liste=_irepo.findById(id);
        if (liste.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(liste.get(),HttpStatus.OK);
    }
    public ResponseEntity<?> deleteCandidat(String id){

        Optional<Candidat> liste=_irepo.findById(id);
        if (liste.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        _irepo.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<?> addCandidat(Candidat c1){
        List<Candidat> candidatCin=_irepo.findByCin(c1.getCin());
        List<Candidat> candidatPassport=_irepo.findByPassport(c1.getPassport());
        List<Candidat> candidatEmail=_irepo.findByEmail(c1.getEmail());


        if(!candidatCin.isEmpty() ||!candidatPassport.isEmpty() || !candidatEmail.isEmpty())
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        c1.calculescore();

        _irepo.save(c1);
        return new ResponseEntity<>(c1,HttpStatus.OK);

    }
    public ResponseEntity<?> updateCandidat(String id, Candidat newCandidat){
    Optional<Candidat> cnd =_irepo.findById(id);

        if(cnd.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        List<Candidat> VerifCin=_irepo.findByCin(newCandidat.getCin());
       Candidat candidat1= cnd.get();

        if(!VerifCin.isEmpty()&&!(VerifCin.get(0).get_id().equals(id)))
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        List<Candidat> VerifPassport=_irepo.findByPassport(newCandidat.getPassport());
        if(!VerifPassport.isEmpty()&&!(VerifPassport.get(0).get_id().equals(id)))
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        List<Candidat> VerifEmail=_irepo.findByEmail(newCandidat.getEmail());
        if(!VerifEmail.isEmpty()&&!(VerifEmail.get(0).get_id().equals(id)))
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        candidat1.setNom(newCandidat.getNom());
        candidat1.setPrenom(newCandidat.getPrenom());
        candidat1.setCin(newCandidat.getCin());
        candidat1.setEmail(newCandidat.getEmail());
        candidat1.setEmail(newCandidat.getPassport());
        candidat1.setDate_Naissance(newCandidat.getDate_Naissance());
        candidat1.setLieu_Naissance(newCandidat.getLieu_Naissance());
        candidat1.setLieu_Residence(newCandidat.getLieu_Residence());
        candidat1.setPhoto(newCandidat.getPhoto());
        candidat1.setPartie_Actuel(newCandidat.getPartie_Actuel());
        candidat1.setScore(newCandidat.getScore());
        candidat1.setListe(newCandidat.getListe());


        candidat1.getAutres_Nationalite().clear();
        candidat1.getEducations().clear();
        candidat1.getActivites().clear();
        candidat1.getReseaux_Sociaux().clear();
        candidat1.getHistoriques_Parties().clear();
        candidat1.getReclamations().clear();
        candidat1.getNote().clear();
        for(var x:newCandidat.getNote()){
            candidat1.getNote().add(x);
        }

        for(var x:newCandidat.getAutres_Nationalite()){
            candidat1.getAutres_Nationalite().add(x);
        }
        for(var x:newCandidat.getEducations()){
            candidat1.getEducations().add(x);
        }
        for(var x:newCandidat.getActivites()){
            candidat1.getActivites().add(x);
        }
        for(var x:newCandidat.getReseaux_Sociaux()){
            candidat1.getReseaux_Sociaux().add(x);
        }
        for(var x:newCandidat.getHistoriques_Parties()){
            candidat1.getHistoriques_Parties().add(x);
        }
        for(var x:newCandidat.getReclamations()){
            candidat1.getReclamations().add(x);
        }
        candidat1.calculescore();
        Candidat c1=_irepo.save(candidat1);
        return new ResponseEntity(c1,HttpStatus.OK);

    }







}
