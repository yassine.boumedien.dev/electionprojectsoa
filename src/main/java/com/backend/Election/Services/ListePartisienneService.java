package com.backend.Election.Services;

import com.backend.Election.Models.ListesElectoral.ListeElectoral;
import com.backend.Election.Models.ListesElectoral.ListePartisienne;
import com.backend.Election.Repos.IRepoListeElectoral;
import com.backend.Election.Repos.IRepoListePartisienne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ListePartisienneService {
    @Autowired
    private IRepoListePartisienne _irepo ;
    @Autowired
    private IRepoListeElectoral _electoralRepos;

    public ListePartisienneService(IRepoListePartisienne irepo,IRepoListeElectoral electoralRepos) {
        _irepo=irepo;
        _electoralRepos=electoralRepos;
    }
    public ResponseEntity<?> GetAll() {
        List<ListePartisienne> liste =_irepo.findAll();

        return new ResponseEntity<>(liste,HttpStatus.OK) ;
    }
    public ResponseEntity<?> Delete (String id) {
        Optional<ListePartisienne> liste=_irepo.findById(id);
        if (liste.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        _irepo.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    public ResponseEntity<?> GetById(String id){
        Optional<ListePartisienne> liste=_irepo.findById(id);
        if (liste.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(liste,HttpStatus.OK);
    }
    public ResponseEntity<?> updateListePartisienne(String id , ListePartisienne newliste)
    {
        Optional<ListePartisienne> liste=_irepo.findById(id);
        if(liste.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        List<ListeElectoral> listverif=_electoralRepos.findByNameAndGov(newliste.getNom(),newliste.getGouvernorat());
        ListePartisienne liste1=liste.get();
        if (!listverif.isEmpty() &&!(listverif.get(0).get_id().equals(id)))
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        liste1.setNom(newliste.getNom());
        liste1.setGouvernorat(newliste.getGouvernorat());
        liste1.setScore(newliste.getScore());
        liste1.setType(newliste.getType());
        liste1.getSupports().clear();
        liste1.getReclamations().clear();
        liste1.getCandidats().clear();

        for(var x:newliste.getSupports())
        {
            liste1.getSupports().add(x);
        }
        for(var x:newliste.getReclamations())
    {
        liste1.getReclamations().add(x);
    }
        for(var x:newliste.getCandidats())
        {
            liste1.getCandidats().add(x);
        }
        ListePartisienne lp=_irepo.save(liste1);
        return new ResponseEntity(lp,HttpStatus.OK);
    }
    public ResponseEntity<?> addListePartisienne(ListePartisienne li)
    {
        List<ListeElectoral> listeElectoral= _electoralRepos.findByNameAndGov(li.getNom(),li.getGouvernorat());

        if(!listeElectoral.isEmpty())
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        ListePartisienne liste1=_irepo.save(li);
        return new ResponseEntity(liste1,HttpStatus.CREATED);
    }



}
