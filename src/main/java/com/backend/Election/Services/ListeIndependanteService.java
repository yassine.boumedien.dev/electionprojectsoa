package com.backend.Election.Services;

import com.backend.Election.Models.ListesElectoral.ListeElectoral;
import com.backend.Election.Models.ListesElectoral.ListeIndependante;
import com.backend.Election.Repos.IRepoListeElectoral;
import com.backend.Election.Repos.IRepoListeIndependante;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class ListeIndependanteService {
    @Autowired
    private IRepoListeIndependante listeindrep;
    @Autowired

    private IRepoListeElectoral electoralRepos;
    public ListeIndependanteService(IRepoListeIndependante listeindrep,IRepoListeElectoral electoralRepos)
    {
        this.listeindrep=listeindrep;
        this.electoralRepos=electoralRepos;
    }
    public ResponseEntity<Collection<ListeElectoral>> GetAllListeIndep(){
        Collection<ListeIndependante> listindep=listeindrep.findAll();
        if(listindep.isEmpty())
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        return new ResponseEntity(listindep, HttpStatus.OK);
    }
    public ResponseEntity<?> getListIndepById(String id)
    {
        Optional<ListeIndependante> listeIndependante=listeindrep.findById(id);
        if(listeIndependante.isEmpty())
           return new ResponseEntity<>(listeIndependante,HttpStatus.OK);
        return new ResponseEntity<>(listeIndependante,HttpStatus.OK);
    }
    public ResponseEntity<ListeIndependante> deleteListeIndep(String id)
    {
        Optional<ListeIndependante> listeIndependante=listeindrep.findById(id);
        if(listeIndependante.isEmpty())
           return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        listeindrep.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    public ResponseEntity<ListeIndependante> addListeIndep(ListeIndependante l1)
    {
        List<ListeElectoral> listeElectoral= electoralRepos.findByNameAndGov(l1.getNom(),l1.getGouvernorat());

        if(!listeElectoral.isEmpty())
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

                ListeIndependante listeIndependante1=listeindrep.save(l1);
                return new ResponseEntity(listeIndependante1,HttpStatus.CREATED);
    }

    public ResponseEntity<ListeIndependante> updateListeIndep(String id, ListeIndependante newListeIndependante)
    {
        Optional<ListeIndependante> listeIndependante=listeindrep.findById(id);

        if(listeIndependante.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        List<ListeElectoral> listeVerif=electoralRepos.findByNameAndGov(newListeIndependante.getNom(),newListeIndependante.getGouvernorat());
        ListeIndependante listeIndependante1=listeIndependante.get();
        if(!listeVerif.isEmpty() &&!(listeVerif.get(0).get_id().equals(id)))
            return new ResponseEntity(HttpStatus.BAD_REQUEST);


        ListeIndependante ListeIndependante1=listeIndependante.get();
        ListeIndependante1.setNom(newListeIndependante.getNom());
        ListeIndependante1.setGouvernorat(newListeIndependante.getGouvernorat());
        ListeIndependante1.setScore(newListeIndependante.getScore());
        ListeIndependante1.setType(newListeIndependante.getType());

        ListeIndependante1.getCandidats().clear();
        ListeIndependante1.getSupports().clear();
        ListeIndependante1.getReclamations().clear();

                for(var x:newListeIndependante.getCandidats())
                {
                    ListeIndependante1.getCandidats().add(x);
                }
                for(var x:newListeIndependante.getSupports())
                {
                    ListeIndependante1.getSupports().add(x);
                }
                for(var x:newListeIndependante.getReclamations())
                 {
                    ListeIndependante1.getReclamations().add(x);
                 }
                ListeIndependante l=listeindrep.save(ListeIndependante1);
                return new ResponseEntity(l,HttpStatus.OK);


    }

}
