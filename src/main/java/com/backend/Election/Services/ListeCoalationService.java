package com.backend.Election.Services;

import com.backend.Election.Models.ListesElectoral.ListeCoalation;
import com.backend.Election.Models.ListesElectoral.ListeElectoral;
import com.backend.Election.Repos.IRepoListeCoalation;
import com.backend.Election.Repos.IRepoListeElectoral;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class ListeCoalationService {
    @Autowired
    private IRepoListeCoalation coaRepos;
    @Autowired
    private IRepoListeElectoral electoralRepos;

    public ListeCoalationService(IRepoListeCoalation coaRepos,IRepoListeElectoral electoralRepos) {
        this.coaRepos = coaRepos;
        this.electoralRepos=electoralRepos;
    }

    public ResponseEntity<Collection<ListeElectoral>> getAllListeCoalation(){
        Collection<ListeCoalation> listeCoalation1= (Collection<ListeCoalation>) coaRepos.findAll();
        if(listeCoalation1.isEmpty())
           return  new ResponseEntity(HttpStatus.NOT_FOUND);
        return new ResponseEntity(listeCoalation1,HttpStatus.OK);
    }
    public ResponseEntity<?> getListeCoalationById(String id) {
        System.out.print(id);
        Optional<ListeCoalation> listeCoalation1 = coaRepos.findById(id);
        if (listeCoalation1.isEmpty())
           return  new ResponseEntity(HttpStatus.NOT_FOUND);
        return new ResponseEntity(listeCoalation1, HttpStatus.OK);
    }
    public ResponseEntity<ListeCoalation> deleteListeCoalation(String id){
        Optional<ListeElectoral> listeCoalation1= electoralRepos.findById(id);
        if (listeCoalation1.isEmpty())
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        coaRepos.deleteById(id);
        return new
                ResponseEntity<>(HttpStatus.OK);
    }
    public ResponseEntity<ListeCoalation> addListeCoalation(ListeCoalation l1){
        List<ListeElectoral> listeElectoral= electoralRepos.findByNameAndGov(l1.getNom(),l1.getGouvernorat());
        if(!listeElectoral.isEmpty())
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        coaRepos.save(l1);
        return new ResponseEntity(l1,HttpStatus.OK);
    }

    public ResponseEntity<ListeCoalation> updateListeCoalation(String id,ListeCoalation newListeCoalation){

        Optional<ListeCoalation> listeCoalation=coaRepos.findById(id);


        if(listeCoalation.isEmpty())
            return new ResponseEntity(HttpStatus.NOT_FOUND);

        List<ListeElectoral> listeVerif=electoralRepos.findByNameAndGov(newListeCoalation.getNom(),newListeCoalation.getGouvernorat());
        ListeCoalation listeCoalation1= listeCoalation.get();

        if(!listeVerif.isEmpty()&&!(listeVerif.get(0).get_id().equals(id)))
            return new ResponseEntity(HttpStatus.BAD_REQUEST);


        listeCoalation1.setNom(newListeCoalation.getNom());
        listeCoalation1.setGouvernorat(newListeCoalation.getGouvernorat());
        listeCoalation1.setScore(newListeCoalation.getScore());
        listeCoalation1.setType(newListeCoalation.getType());
        listeCoalation1.getCandidats().clear();
        listeCoalation1.getSupports().clear();
        listeCoalation1.getReclamations().clear();
        listeCoalation1.getParties().clear();

        for(var x:newListeCoalation.getCandidats()){
            listeCoalation1.getCandidats().add(x);
        }
        for(var x:newListeCoalation.getSupports()){
            listeCoalation1.getSupports().add(x);
        }
        for(var x:newListeCoalation.getReclamations()){
            listeCoalation1.getReclamations().add(x);
        }
        for(var x:newListeCoalation.getParties()){
            listeCoalation1.getParties().add(x);
        }
        ListeCoalation l1=coaRepos.save(listeCoalation1);
        return new ResponseEntity(l1,HttpStatus.OK);

    }

}
