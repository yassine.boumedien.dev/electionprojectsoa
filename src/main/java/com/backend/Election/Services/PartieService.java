package com.backend.Election.Services;




import com.backend.Election.Models.Partie;
import com.backend.Election.Repos.IRepoPartie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service

public class PartieService {
    @Autowired
    private IRepoPartie _irepo;

    public PartieService(IRepoPartie irepo) {
        _irepo = irepo;
    }

    public ResponseEntity<?> GetAllPartie() {
        List<Partie> list = _irepo.findAll();
        if (list.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    public ResponseEntity<?> GetPartieById(String id) {
        Optional<Partie> liste = _irepo.findById(id);
        if (liste.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(liste, HttpStatus.OK);
    }

    public ResponseEntity<?> DeletePartie(String id) {

        Optional<Partie> liste = _irepo.findById(id);
        if (liste.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        _irepo.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<?> AddPartie(Partie p1) {
        List<Partie> liste = _irepo.findByName(p1.getNom());
        if (!liste.isEmpty())
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        List<Partie> liste1 = _irepo.findByPresident(p1.getPresident());
        if (!liste1.isEmpty())
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        _irepo.save(p1);
        return new ResponseEntity(p1, HttpStatus.OK);
    }

    public ResponseEntity<Partie> updatePartie(String id, Partie newPartie) {

        Optional<Partie> liste = _irepo.findById(id);


        if (liste.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        List<Partie> listverif = _irepo.findByName(newPartie.getNom());


        if (!listverif.isEmpty() && !(listverif.get(0).get_id().equals(id)))
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        List<Partie> listverif1 = _irepo.findByPresident(newPartie.getPresident());
        if (!listverif1.isEmpty() && !(listverif1.get(0).get_id().equals(id)))
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        var list = liste.get();
        list.setNom(newPartie.getNom());
        list.setPresident(newPartie.getPresident());
        list.setDate_Fondation(newPartie.getDate_Fondation());
        if (list.getSupports()!=null){
            list.getSupports().clear();
            for (var x : newPartie.getSupports()) {
                list.getSupports().add(x);
            }
        }
        if (list.getFondateurs()!=null){
            list.getFondateurs().clear();


            for (var x : newPartie.getFondateurs()) {
                list.getFondateurs().add(x);
            }
        }

        Partie p1 = _irepo.save(list);
        return new ResponseEntity(p1, HttpStatus.OK);


    }
}
