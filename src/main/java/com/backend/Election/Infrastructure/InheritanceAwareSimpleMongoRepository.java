package com.backend.Election.Infrastructure;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.repository.query.MongoEntityInformation;
import org.springframework.data.mongodb.repository.support.SimpleMongoRepository;

public class InheritanceAwareSimpleMongoRepository<T, ID extends Serializable> extends SimpleMongoRepository<T, ID> {

    private final MongoOperations mongoOperations;
    private final MongoEntityInformation<T, ID> entityInformation;
    private final Document classCriteriaDocument;
    private final Criteria classCriteria;

    public InheritanceAwareSimpleMongoRepository(MongoEntityInformation<T, ID> metadata,
                                                 MongoOperations mongoOperations) {
        super(metadata, mongoOperations);
        this.mongoOperations = mongoOperations;
        this.entityInformation = metadata;

        if (entityInformation.getJavaType().isAnnotationPresent(TypeAlias.class)) {
            classCriteria = where("_class").is(entityInformation.getJavaType().getAnnotation(TypeAlias.class).value());
            classCriteriaDocument = classCriteria.getCriteriaObject();
        } else {
            classCriteriaDocument = new Document();
            classCriteria = null;
        }
    }

    @Override
    public long count() {
        return classCriteria != null ? mongoOperations.getCollection(
                entityInformation.getCollectionName()).countDocuments(
                classCriteriaDocument)
                : super.count();
    }
    /*@Override
    public Optional<T> findById (ID Id) {
        System.out.print("ObjectId("+'"'+Id.toString()+'"'+")");

        Criteria c= where("_id").is("ObjectId("+'"'+Id.toString()+'"'+")");
        Document dec = c.getCriteriaObject();

        try{
            T data=mongoOperations.findOne(new Query().addCriteria(c),
                    entityInformation.getJavaType());
            System.out.print(data.toString());


        }
        catch (Exception e){
            System.out.print(e.getMessage());

        }


       ///Optional<T> d=Optional.of(data);
        Optional<T> d=Optional.empty();
        return null;

    }
*/

    @Override
    public List<T> findAll() {
        return classCriteria != null ? mongoOperations.find(new Query().addCriteria(classCriteria),
                entityInformation.getJavaType(),
                entityInformation.getCollectionName())
                : super.findAll();
    }
}