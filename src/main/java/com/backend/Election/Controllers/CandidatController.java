package com.backend.Election.Controllers;

import com.backend.Election.Models.Candidat;
import com.backend.Election.Services.CandidatService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/candidat")
@CrossOrigin(origins = "http://localhost:4200")

public class CandidatController {
    @Autowired
    private CandidatService cndService;
    private final Logger log= LoggerFactory.getLogger(Candidat.class);

    @GetMapping("/findall")
    public ResponseEntity<?> getAllCandidat(){
        return cndService.getAllCandidat();
    }

    @GetMapping("/findbyid/{Id}")
    public ResponseEntity<?> getCandidatById(@PathVariable String Id){
        System.out.println(Id);
        return cndService.getCandidatById(Id);
    }

    @PostMapping("/add")
    public ResponseEntity<?> addCandidat (@RequestBody Candidat c1){
        log.info("Ajouter Candidat",c1);
        return cndService.addCandidat(c1);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateCandidat(@PathVariable String id, @Valid @RequestBody Candidat c1){
        return cndService.updateCandidat(id, c1);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteCandidat(@PathVariable String id){
        return cndService.deleteCandidat(id);
    }
}
