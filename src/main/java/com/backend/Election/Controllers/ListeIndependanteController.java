package com.backend.Election.Controllers;

import com.backend.Election.Models.ListesElectoral.ListeElectoral;
import com.backend.Election.Models.ListesElectoral.ListeIndependante;
import com.backend.Election.Services.ListeIndependanteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("/listeindependante")
@CrossOrigin(origins = "http://localhost:4200")

public class ListeIndependanteController {
    @Autowired
    private ListeIndependanteService indepservice;
    private final Logger log = LoggerFactory.getLogger(ListeIndependante.class);
    @GetMapping("/findall")
    public ResponseEntity<Collection<ListeElectoral>> GetAllListeIndep()
    {
        return indepservice.GetAllListeIndep();
    }
    @GetMapping("/findbyid/{id}")
    public ResponseEntity<?>getListIndepById(@PathVariable String id)
    {
        return indepservice.getListIndepById(id);
    }
    @PostMapping("/add")
    public ResponseEntity<ListeIndependante> addListeIndep(@Valid @RequestBody ListeIndependante l1)
    {
        return indepservice.addListeIndep(l1);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ListeIndependante> deleteListeIndep(@PathVariable String id)
    {
        return indepservice.deleteListeIndep(id);
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<ListeIndependante> updateListeIndep(@PathVariable String id, @Valid @RequestBody ListeIndependante l1)
    {
        return indepservice.updateListeIndep(id,l1);
    }

}
