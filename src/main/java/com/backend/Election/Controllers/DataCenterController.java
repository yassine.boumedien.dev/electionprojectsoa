package com.backend.Election.Controllers;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/datacenter")

@RestController
public class DataCenterController {
    List<String> files = new ArrayList<String>();
    private final Path rootLocation = Paths.get("./uploads/");

    @PostMapping("/savefile")
    public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file) {
        String message;
        String name=(new Date().getTime())+""+file.getOriginalFilename();
        try {
            try {
                Files.copy(file.getInputStream(), this.rootLocation.resolve(name));
            } catch (Exception e) {
                System.out.println("STUCK HERE IN 1 EXCEPT");

                throw new RuntimeException("FAIL!");
            }
            files.add(file.getOriginalFilename());

            message = "Successfully uploaded!";
            return ResponseEntity.status(HttpStatus.OK).body(name);
        } catch (Exception e) {
            message = "Failed to upload!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }
    @GetMapping("/download/{path}")
    public ResponseEntity<Resource> download(@PathVariable  String path)  {

        // ...
        java.io.File file2Upload = new File("./uploads/"+path);
        try{
            InputStreamResource resource = new InputStreamResource(new FileInputStream(file2Upload));
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType("application/pdf"))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);
        }
        catch( FileNotFoundException e){
            return  new ResponseEntity(HttpStatus.NOT_FOUND);
        }


    }
}
