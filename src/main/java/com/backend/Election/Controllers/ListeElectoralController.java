package com.backend.Election.Controllers;

import com.backend.Election.Models.ListesElectoral.ListeCoalation;
import com.backend.Election.Services.ListeElectoralService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/listeelectoral")

public class ListeElectoralController {
    @Autowired
    private ListeElectoralService _listeElectoralservice;
    private final Logger log = LoggerFactory.getLogger(ListeCoalation.class);

    @GetMapping("/findall")
    public ResponseEntity<?> GetAll(){
        return _listeElectoralservice.GetAll();
    }



    @GetMapping("/findbyid/{Id}")
    public ResponseEntity<?> GetById(@PathVariable String Id){
        return _listeElectoralservice.GetById(Id);
    }

    @DeleteMapping("/delete/{Id}")
    public ResponseEntity<?> Delete(@PathVariable String Id){
        return _listeElectoralservice.Delete(Id);
    }
}
