package com.backend.Election.Controllers;

import com.backend.Election.Models.ListesElectoral.ListeCoalation;
import com.backend.Election.Models.ListesElectoral.ListePartisienne;
import com.backend.Election.Services.ListePartisienneService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("listepartisienne")
@CrossOrigin(origins = "http://localhost:4200")

public class ListePartisienneController {
    @Autowired
    private ListePartisienneService _listePartisienneService;
    private final Logger log = LoggerFactory.getLogger(ListeCoalation.class);

    @GetMapping("/findall")
    public ResponseEntity<?> getAllListeCoalation(){
        return _listePartisienneService.GetAll();
    }

    @GetMapping("/findbyid/{Id}")
    public ResponseEntity<?> getCoalationByID(@PathVariable String Id){

        return _listePartisienneService.GetById(Id);
    }

    @PostMapping("/add")
    public ResponseEntity<?> addListeCoalation (@RequestBody ListePartisienne l1){
        log.info("Ajouter Liste Partisienne",l1);
        return _listePartisienneService.addListePartisienne(l1);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> modifyListeCoalation(@PathVariable String id,
                                                  @Valid @RequestBody ListePartisienne l1){
        return _listePartisienneService.updateListePartisienne(id, l1);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteListeCoalation(@PathVariable String id){
        return _listePartisienneService.Delete(id);
    }
    
}
