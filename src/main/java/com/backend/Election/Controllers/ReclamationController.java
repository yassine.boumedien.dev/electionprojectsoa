package com.backend.Election.Controllers;

import com.backend.Election.Models.Utils.Reclamation;
import com.backend.Election.Services.ReclamationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/Reclamation")
public class ReclamationController {
    @Autowired
    private ReclamationService recService;
    private final Logger log= LoggerFactory.getLogger(Reclamation.class);

    @GetMapping("/findall")
    public ResponseEntity<?> getAllReclamation(){
        return recService.getAllReclamation();
    }

    @GetMapping("/findbyid/{Id}")
    public ResponseEntity<?> getReclamationById(@PathVariable String Id){

        return recService.getReclamationById(Id);
    }
    @PostMapping("/add")
    public ResponseEntity<?> addReclamation (@RequestBody Reclamation r1){
        log.info("Ajouter Reclamation",r1);
        return recService.addReclamation(r1);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> UpdateReclamation(@PathVariable String id, @Valid @RequestBody Reclamation r1){
        return recService.updateReclamation(id, r1);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteReclamation(@PathVariable String id){
        return recService.deleteReclamation(id);
    }
}
