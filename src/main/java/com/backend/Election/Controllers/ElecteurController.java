package com.backend.Election.Controllers;
import com.backend.Election.Models.Users.Electeur;
import com.backend.Election.Services.ElecteurService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/electeur")
public class ElecteurController {
    @Autowired
    private ElecteurService elecService;
    private final Logger log= LoggerFactory.getLogger(Electeur.class);

    @GetMapping("/findall")
         public ResponseEntity<?> getAllElecteur(){
             return elecService.getAllElecteur();
         }

    @GetMapping("/findbyid/{Id}")
    public ResponseEntity<?> getElecteurById(@PathVariable String Id){

        return elecService.getElecteurById(Id);
    }

    @PostMapping("/add")
    public ResponseEntity<?> addElecteur (@RequestBody Electeur e1){
        log.info("Ajouter Electeur",e1);
        return elecService.addElecteur(e1);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> UpdateElecteur(@PathVariable String id, @Valid @RequestBody Electeur e1){
        return elecService.updateElecteur(id, e1);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteElecteur(@PathVariable String id){
        return elecService.deleteElecteur(id);
    }


}
