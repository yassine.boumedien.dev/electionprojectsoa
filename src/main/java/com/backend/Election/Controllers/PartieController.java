package com.backend.Election.Controllers;


import com.backend.Election.Models.Partie;
import com.backend.Election.Services.PartieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/partie")
@CrossOrigin(origins = "http://localhost:4200")

public class PartieController {

    @Autowired
    private PartieService _irepo;
    private final Logger log = LoggerFactory.getLogger(Partie.class);

    @GetMapping("/findall")
    public ResponseEntity<?> getAllPartie() {
        return _irepo.GetAllPartie();
    }

    @GetMapping("/findbyid/{Id}")
    public ResponseEntity<?> getPartieByID(@PathVariable String Id) {

        return _irepo.GetPartieById(Id);
    }

    @PostMapping("/add")
    public ResponseEntity<?> AddPartie(@RequestBody Partie p1) {
        log.info("Ajouter partie", p1);
        return _irepo.AddPartie(p1);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> DeletePartie(@PathVariable String id) {
        return _irepo.DeletePartie(id);
    }
        @PutMapping("/update/{id}")
        public ResponseEntity<?> updatePartie (@PathVariable String id,@Valid @RequestBody Partie p1){
            return _irepo.updatePartie(id, p1);
        }
    }

