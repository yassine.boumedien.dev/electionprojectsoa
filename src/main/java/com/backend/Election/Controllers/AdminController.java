package com.backend.Election.Controllers;


import com.backend.Election.Models.Users.Admin;
import com.backend.Election.Services.AdminService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/Admin")
public class AdminController {
    @Autowired
    private AdminService adminService;
    private final Logger log = LoggerFactory.getLogger(Admin.class);
    @GetMapping("/findall")
    public ResponseEntity<?> getAllAdmins(){
        return adminService.getAll();
    }

    @GetMapping("/findbyid/{Id}")
    public ResponseEntity<?> getAdminById(@PathVariable String Id){

        return adminService.getById(Id);
    }

    @PostMapping("/add")
    public ResponseEntity<?> addAdmin (@RequestBody Admin a1){
        log.info("Ajouter Admin ",a1);
        return adminService.addAdmin(a1);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> modifyAdmin(@PathVariable String id,
                                                  @Valid @RequestBody Admin a1){
        return adminService.updateAdmin(id, a1);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteAdmin(@PathVariable String id){
        return adminService.deleteAdmin(id);
    }

}
