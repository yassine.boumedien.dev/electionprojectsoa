package com.backend.Election.Controllers;

import com.backend.Election.Models.ListesElectoral.ListeCoalation;
import com.backend.Election.Services.ListeCoalationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/listecoalation")
@CrossOrigin(origins = "http://localhost:4200")

public class ListeCoalationController {

    @Autowired
    private ListeCoalationService coaService;
    private final Logger log = LoggerFactory.getLogger(ListeCoalation.class);
        @GetMapping("/findall")
        public ResponseEntity<?> getAllListeCoalation(){
            return coaService.getAllListeCoalation();
        }

        @GetMapping("/findbyid/{Id}")
        public ResponseEntity<?> getCoalationByID(@PathVariable String Id){

            return coaService.getListeCoalationById(Id);
        }

        @PostMapping("/add")
        public ResponseEntity<?> addListeCoalation (@RequestBody ListeCoalation l1){
            log.info("Ajouter Liste Coalation",l1);
            return coaService.addListeCoalation(l1);
        }

        @PutMapping("/update/{id}")
        public ResponseEntity<?> modifyListeCoalation(@PathVariable String id,
                                                             @Valid @RequestBody ListeCoalation l1){
            return coaService.updateListeCoalation(id, l1);
        }

        @DeleteMapping("/delete/{id}")
        public ResponseEntity<?> deleteListeCoalation(@PathVariable String id){
            return coaService.deleteListeCoalation(id);
        }






}
