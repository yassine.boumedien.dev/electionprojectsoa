package com.backend.Election.Models;


import com.backend.Election.Models.Utils.Reclamation;
import com.backend.Election.Models.Utils.Support;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document
@Data
@AllArgsConstructor
public class Partie {
    @org.springframework.data.annotation.Id
    private String _id;
    private String Nom;
    private Date Date_Fondation;
    private List<String> Fondateurs=new ArrayList<>();
    private String President;
    private List<Support> Supports=new ArrayList<>();


}