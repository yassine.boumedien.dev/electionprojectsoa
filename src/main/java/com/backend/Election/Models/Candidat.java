package com.backend.Election.Models;

import com.backend.Election.Models.CandidatCV.Activite;
import com.backend.Election.Models.Utils.Note;
import com.backend.Election.Models.CandidatCV.Education;
import com.backend.Election.Models.CandidatCV.HistoriquePartie;
import com.backend.Election.Models.CandidatCV.Nationalite;
import com.backend.Election.Models.ListesElectoral.ListeElectoral;
import com.backend.Election.Models.Utils.Note;
import com.backend.Election.Models.Utils.Reclamation;
import com.backend.Election.Models.Utils.ReseauSocial;
import com.backend.Election.Models.Utils.Support;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Candidat {
    @Id
    private String _id;
    private String Nom;
    private String Prenom;
    private String Cin;
    private String Passport;
    private String Email;
    private Date Date_Naissance;
    private String Lieu_Naissance;
    private String Lieu_Residence;
    private List<Nationalite> Autres_Nationalite=new ArrayList<>() ;
    private List<Education> Educations=new ArrayList<>() ;
    private List<Activite> Activites=new ArrayList<>() ;
    private List<ReseauSocial> Reseaux_Sociaux=new ArrayList<>() ;
    private Support Photo;
    private List<HistoriquePartie> Historiques_Parties=new ArrayList<>() ;
    @DBRef
    private Partie Partie_Actuel;
    private double Score;
    private List<Note> Note=new ArrayList<>();
    @DBRef
    private ListeElectoral Liste;
    private List<Reclamation> Reclamations=new ArrayList<>() ;



    public void calculescore(){
        double score=0;
        for (Note n:Note){
            score+=n.getNote();
        }
        for (Education n:Educations){
            score+=n.getDateFin().getYear()-n.getDateDebut().getYear()+1;
        }
        for (Activite n:Activites){
            score+=n.getDateFin().getYear()-n.getDateDebut().getYear()+1;
        }
        this.Score=score;
    }
}

