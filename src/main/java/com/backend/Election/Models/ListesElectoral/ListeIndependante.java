package com.backend.Election.Models.ListesElectoral;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
@Data

@Document(collection = "ListeElectoral")
@TypeAlias("Indepandante")

public class ListeIndependante extends ListeElectoral {
}
