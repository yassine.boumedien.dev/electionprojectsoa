package com.backend.Election.Models.ListesElectoral;

import com.backend.Election.Models.Partie;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Data

@Document(collection = "ListeElectoral")
@TypeAlias("Coalation")

public class ListeCoalation extends ListeElectoral {
    @DBRef
    private List<Partie> Parties=new ArrayList<>();
}
