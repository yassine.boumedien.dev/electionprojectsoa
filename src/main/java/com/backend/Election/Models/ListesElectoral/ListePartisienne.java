package com.backend.Election.Models.ListesElectoral;

import com.backend.Election.Models.Partie;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
@Data

@Document(collection = "ListeElectoral")
@TypeAlias("Partisienne")

public class ListePartisienne extends ListeElectoral {
    @DBRef
    private Partie Partie;
}
