package com.backend.Election.Models.ListesElectoral;

import com.backend.Election.Models.CandidatElectoral;
import com.backend.Election.Models.Partie;
import com.backend.Election.Models.Utils.Reclamation;
import com.backend.Election.Models.Utils.Support;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "ListeElectoral")
public   class ListeElectoral {
    @Id

    private String _id;


    private String Nom;
    private String Gouvernorat;
    private List<CandidatElectoral> Candidats=new ArrayList<>() ;
    private double Score;
    private String Type;

    private List<Support> Supports=new ArrayList<>() ;
    private List<Reclamation> Reclamations=new ArrayList<>() ;

}
