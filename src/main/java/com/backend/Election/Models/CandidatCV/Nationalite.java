package com.backend.Election.Models.CandidatCV;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@AllArgsConstructor
@Data
public class Nationalite {
    private String Pays;
    private String Titre;
    private Date DateDebut;
    private Date DateFin;
}
