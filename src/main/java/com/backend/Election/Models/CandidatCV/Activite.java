package com.backend.Election.Models.CandidatCV;

import com.backend.Election.Models.Utils.Support;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Activite {
    private String Title;
    private String Type;
    private String Description;
    private Date DateDebut;
    private Date DateFin;
    private List<Support> Supports=new ArrayList<>();

}
