package com.backend.Election.Models.CandidatCV;

import com.backend.Election.Models.Partie;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.Date;

@AllArgsConstructor
@Data
public class HistoriquePartie {
    private String Poste;
    private Date DateDebut;
    private Date Datefin;
    private String Remarques;
    @DBRef
    private Partie Partie;
}
