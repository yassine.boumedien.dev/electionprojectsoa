package com.backend.Election.Models.CandidatCV;

import com.backend.Election.Models.Utils.Support;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class Education {
    private String Institut;
    private String Specialite;
    private String Diplome;
    private Date DateDebut;
    private Date DateFin;
    private String Description;
    private List<Support> Supports;

}
