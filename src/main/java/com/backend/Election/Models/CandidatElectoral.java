package com.backend.Election.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.DBRef;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CandidatElectoral {
    @DBRef
    private Candidat Candidat;
    private int Position;
}
