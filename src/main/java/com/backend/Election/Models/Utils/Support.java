package com.backend.Election.Models.Utils;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Support {
    private String Titre;
    private String Type;
    private String Path;
    private String Description;
}
