package com.backend.Election.Models.Utils;

import com.backend.Election.Models.Candidat;
import com.backend.Election.Models.Users.Electeur;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Reclamation {
    @org.springframework.data.annotation.Id
    private String _id;
    private String Titre;
    private String Description;
    private String Status="En cours";
    private List<Support> Supports;

    @DBRef
    private Candidat Reclamateur;


}
