package com.backend.Election.Models.Utils;

import com.backend.Election.Models.Candidat;
import com.backend.Election.Models.Users.Electeur;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Note {
    private float note;
    @DBRef
    private com.backend.Election.Models.Users.Electeur Electeur;

}
