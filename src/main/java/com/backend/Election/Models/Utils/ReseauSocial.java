package com.backend.Election.Models.Utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
public class ReseauSocial {
    private String Titre;
    private String Lien;
}
