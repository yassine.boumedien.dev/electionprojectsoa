package com.backend.Election.Models.Users;

import com.backend.Election.Models.Utils.Reclamation;
import com.backend.Election.Models.Utils.Support;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document

public class Electeur {

    @Id
    private String _id;
    private String Nom;
    private String Prenom;
    private String Adresse_Mac;
    //utile lel e7sai2yét
    private Date Date_naissance;



}
