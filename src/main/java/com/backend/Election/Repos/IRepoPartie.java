package com.backend.Election.Repos;
import com.backend.Election.Models.Partie;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface IRepoPartie extends MongoRepository<Partie, String> {
    @Query("{ 'Nom' : ?0 }")
    List<Partie> findByName(String Nom);
    @Query ("{'President':?0}")
    List<Partie> findByPresident(String President) ;
}

