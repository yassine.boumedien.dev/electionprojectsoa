package com.backend.Election.Repos;

import com.backend.Election.Models.ListesElectoral.ListePartisienne;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface IRepoListePartisienne  extends MongoRepository<ListePartisienne, String> {
}
