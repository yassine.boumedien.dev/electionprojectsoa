package com.backend.Election.Repos;

import com.backend.Election.Models.Utils.Reclamation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRepoReclamation extends MongoRepository<Reclamation,String> {

}
