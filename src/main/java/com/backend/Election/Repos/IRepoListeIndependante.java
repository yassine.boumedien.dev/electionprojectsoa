package com.backend.Election.Repos;

import com.backend.Election.Models.ListesElectoral.ListeIndependante;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface IRepoListeIndependante extends MongoRepository<ListeIndependante, String> {
}
