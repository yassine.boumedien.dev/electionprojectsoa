package com.backend.Election.Repos;

import com.backend.Election.Models.ListesElectoral.ListeCoalation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface IRepoListeCoalation extends MongoRepository<ListeCoalation, String> {
    
}
