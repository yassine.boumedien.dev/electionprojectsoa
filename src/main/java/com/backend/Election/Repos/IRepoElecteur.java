package com.backend.Election.Repos;

import com.backend.Election.Models.Users.Electeur;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IRepoElecteur extends MongoRepository<Electeur,String> {
    @Query("{ 'Adresse_Mac' : ?0 ")
    List<Electeur> findByMac(String Adresse_Mac);


}
