package com.backend.Election.Repos;

import com.backend.Election.Models.Users.Admin;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IRepoAdmin extends MongoRepository<Admin,  String> {
    @Query("{ 'Username' : ?0}")
    List<Admin> findByUsername(String Username);
}
