package com.backend.Election.Repos;

import com.backend.Election.Models.Candidat;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.MongoId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IRepoCandidat extends MongoRepository<Candidat, String> {

    @Query("{ 'Cin' : ?0}")
    List<Candidat> findByCin(String Cin);
    @Query("{ 'Passport' : ?0}")
    List<Candidat> findByPassport(String Passport);
    @Query("{ 'Email' : ?0}")
    List<Candidat> findByEmail(String Email);
    @Query("{ '_id' : ?0}")
    List<Candidat> GetById(ObjectId id);
}
