package com.backend.Election.Repos;


import com.backend.Election.Models.ListesElectoral.ListeElectoral;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface IRepoListeElectoral extends MongoRepository<ListeElectoral, String> {
    @Query("{ 'Nom' : ?0 ,'Gouvernorat':?1 }")
    List<ListeElectoral> findByNameAndGov(String Nom, String Gouvernorat);

}
